# My K8s Setup

A repository to manage what runs on my Kubernetes cluster, GitOps style.


# How to set up

First generate a ssh key-pair:
```
ssh-keygen -t ed25519 -C "Gitlab deploy key"
```
I saved it as `/home/andreas/.ssh/id_gl_deploy`

Copy the contents of `id_gl_deploy.pub` into the `ssh_public_key` variable in [variables.tf](variables.tf).

Add a CI/CD vaiable for the private key. 
 - Key: `SSH_PRIVATE_KEY`
 - Value: Content of `id_gl_deploy`
 - Type: File
 - [x] Protect variable

Add a CI/CD vaiable for a gitlab personal access token with `api`, `read_user`, and `read_api` scopes. 
 - Key: `TF_VAR_gitlab_token`
 - Value: The created token
 - Type: Variable
 - [x] Protect variable
 - [x] Mask variable

Add a CI/CD vaiable for your k8s config. 
 - Key: `TF_VAR_kubeconf`
 - Value: Content of your `~/.kube/config`
 - Type: File
 - [x] Protect variable
