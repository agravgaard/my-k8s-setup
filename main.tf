terraform {
  required_version = ">= 0.13"

  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = ">= 3.7.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0.2"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.10.0"
    }
    flux = {
      source  = "fluxcd/flux"
      version = ">= 0.0.13"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "3.1.0"
    }
    gitops = {
      source  = "tyler-technologies/gitops"
      version = "0.0.2-rc"
    }

  }
}

provider "flux" {}

provider "kubectl" {}

provider "kubernetes" {
  config_path = var.kubeconf
}

provider "gitlab" {
  token = var.gitlab_token
}

provider "gitops" {
  repo_url = "git@gitlab.com:${var.gitlab_user}/${var.repository_name}"
  branch   = var.branch
  path     = "tmp.mycheckoutdestination"
}

resource "tls_private_key" "main" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

# Flux
data "flux_install" "main" {
  target_path = var.target_path
}

data "flux_sync" "main" {
  target_path = var.target_path
  url         = "ssh://git@gitlab.com/${var.gitlab_user}/${var.repository_name}.git"
  branch      = var.branch
}

# Kubernetes
resource "kubernetes_namespace" "flux_system" {
  metadata {
    name = "flux-system"
  }

  lifecycle {
    ignore_changes = [
      metadata[0].labels,
    ]
  }
}

data "kubectl_file_documents" "install" {
  content = data.flux_install.main.content
}

data "kubectl_file_documents" "sync" {
  content = data.flux_sync.main.content
}

locals {
  install = [for v in data.kubectl_file_documents.install.documents : {
    data : yamldecode(v)
    content : v
    }
  ]
  sync = [for v in data.kubectl_file_documents.sync.documents : {
    data : yamldecode(v)
    content : v
    }
  ]
}

resource "kubectl_manifest" "install" {
  for_each   = { for v in local.install : lower(join("/", compact([v.data.apiVersion, v.data.kind, lookup(v.data.metadata, "namespace", ""), v.data.metadata.name]))) => v.content }
  depends_on = [kubernetes_namespace.flux_system]
  yaml_body  = each.value
}

resource "kubectl_manifest" "sync" {
  for_each   = { for v in local.sync : lower(join("/", compact([v.data.apiVersion, v.data.kind, lookup(v.data.metadata, "namespace", ""), v.data.metadata.name]))) => v.content }
  depends_on = [kubernetes_namespace.flux_system]
  yaml_body  = each.value
}

resource "kubernetes_secret" "main" {
  depends_on = [kubectl_manifest.install]

  metadata {
    name      = data.flux_sync.main.secret
    namespace = data.flux_sync.main.namespace
  }

  data = {
    identity       = tls_private_key.main.private_key_pem
    "identity.pub" = tls_private_key.main.public_key_pem
    known_hosts    = var.ssh_public_key
  }
}

# Gitlab
resource "gitlab_project" "main" {
  name             = var.repository_name
  visibility_level = var.repository_visibility
  default_branch   = var.branch
}

resource "gitlab_deploy_key" "main" {
  project = gitlab_project.main.id
  title   = "Deploy key for my-k8s-setup"
  key     = var.ssh_public_key
}

resource "gitops_checkout" "test_checkout" {}

resource "gitops_file" "install" {
  checkout = gitops_checkout.test_checkout.id
  path     = data.flux_install.main.path
  contents = data.flux_install.main.content
}

resource "gitops_file" "sync" {
  checkout = gitops_checkout.test_checkout.id
  path     = data.flux_sync.main.path
  contents = data.flux_sync.main.content
}

resource "gitops_file" "kustomize" {
  checkout = gitops_checkout.test_checkout.id
  path     = data.flux_sync.main.kustomize_path
  contents = data.flux_sync.main.kustomize_content
}

resource "gitops_commit" "tf_commit" {
  commit_message = "add flux files through terraform"
  handles        = [gitops_file.install.id, gitops_file.sync.id, gitops_file.kustomize.id]
}
