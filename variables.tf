variable "kubeconf" {
  type        = string
  default     = "~/.kube/config"
  description = "Path to k8s config"
}

variable "ssh_public_key" {
  type        = string
  default     = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEpAh7QAI5oizcnIiRgqi3pDqbzJ9yYcNiGjZ91v4jBx Gitlab deploy key"
  description = "Public ssh key used for Gitlab deploy key"
}

variable "gitlab_token" {
  type        = string
  description = "gitlab token"
}

variable "gitlab_user" {
  type        = string
  default     = "agravgaard"
  description = "gitlab username"
}

variable "repository_name" {
  type        = string
  default     = "my-k8s-setup"
  description = "github repository name"
}

variable "repository_visibility" {
  type        = string
  default     = "public"
  description = "How visible is the gitlab repo"
}

variable "branch" {
  type        = string
  default     = "main"
  description = "branch name"
}

variable "target_path" {
  type        = string
  default     = "clusters/production"
  description = "flux sync target path"
}
