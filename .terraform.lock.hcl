# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/fluxcd/flux" {
  version     = "0.6.1"
  constraints = ">= 0.0.13"
  hashes = [
    "h1:jyFAs9LOMhVjX8jjbUQS2iQFj0Yv4mCRgtu9F0Jhmyw=",
    "zh:015b8e70d11e7d6b707436fbacf41028d251cabaeced05128c6e506932f9dc24",
    "zh:0222e0774de186b5c0eab1749743c92cc272dff701a8685263df3418621dec89",
    "zh:0baedbc3e7875d8a11edc5da50ddad33c35e11387e1ed19f2bf85ec38145b1d7",
    "zh:1e006bd8404d28f35970fe43774af5f824aa32589d5481a4ac4b0b4144a5222c",
    "zh:2cf555d7aad003e5d682dde8af79b367e8f6b019f8866b111a3b13bdf0f88888",
    "zh:3cab05f37902d2dc8902143f586490e8d985e7f72fc37b92ce31fd2b975c7a4d",
    "zh:40f00d64b9466df7498e6c960e127b61c8506c8dbe79e2c606a25daa5e1a57dc",
    "zh:64892b76457d17f9c3d20e0ecac45770e9814be68d292fec51fd840c6c07524c",
    "zh:7f63bf072394989cc60a90c118033682dfc2adc408244f9eb48e581744f4037f",
    "zh:96ef45354e79d5351cd18f7ac0c780ff70b5ac995a8925c82a2e45630bc7ceb3",
    "zh:daf0b70daf10dd1807aa5d16b57cb6d26d22e306918fa91e7f8bed4eed56e255",
    "zh:ee53a34473e8d169d749f1f56c22c6f97d0d2c87c4c63f42b9b08d62e2515989",
    "zh:f47d1928d273d4a92eab9c761a14a12c828feb3a2385b935c13df0f14ba9b7cd",
  ]
}

provider "registry.terraform.io/gavinbunney/kubectl" {
  version     = "1.13.0"
  constraints = ">= 1.10.0"
  hashes = [
    "h1:blP230ezSDEmb3WNUI8kDHCYrykyEucz7N7IZmODd/k=",
    "zh:088c99d7e079ba2be3abe1d5c5b2070eff85256178467783af125d11026f08b6",
    "zh:0d3fa3bfb4768dd39e2f3af4d85e69fdb8f6abcbe92fece37fc78a97dedd7dc1",
    "zh:227d9fb591a0cdcd482410b88c6d91f17922a85fb9caef9b73c2883f6964b483",
    "zh:607bff8e6e03ae2b4d523c21377fa655d370cc8310812310ae61b409e7c271d5",
    "zh:621d46414e23d5a7cfb1ba25275f1cac1fba78be5c1512f0a0614752425411cc",
    "zh:76aace9adb7dc9c10abcc52b31947821335b60b7b335b485bd05f20a91debd63",
    "zh:a9ff1f7c676d89cacd64605ad899749dd718f65cb879fabba8e15fcfd0a07629",
    "zh:b122fa06ad1978ec3092cce48f16456aa820bf5786a101a8378323659ed11db3",
    "zh:fcf5ad18fafe717739c5d40d8c4e4a70e123cf4296efc7286f9d98e3c42e410f",
  ]
}

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "3.7.0"
  constraints = ">= 3.7.0"
  hashes = [
    "h1:Xe8KHFOG3zA61RjvDtVgWKCPeT4l1++XEGVJyNEeeM4=",
    "zh:16addba6bda82e6689d21049392d296ce276cb93cbc5bcf3ad21f7d39cd820fd",
    "zh:1e9dd3db81a38d3112ddb24cec5461d919e7770a431f46ac390469e448716fd4",
    "zh:252c08d473d938c2da2711838db2166ecda2a117f64a10d279e935546ef7d991",
    "zh:2e0c83da0ba44e0521cb477dd60ea7c08b9edbc44b607e5ddb963becb26970a5",
    "zh:396223391782f1f809a60f045bfdcde41820d0d6119912718d86fc153fc28969",
    "zh:3a6b3c0901b81bc451d1ead2a2f26845d5db6b6253758c1f0aa0bad53fb6b4bd",
    "zh:51010e8f1d05f4979f0e10cf0e3b56cec13c731d99f373dda9fd9561ddb2826b",
    "zh:53ef55edf7698cbb4562265a6ab9e261716f62a82590e5fb6ad4f7f7458bdc5c",
    "zh:6c2db10e6226cc748e6dd5c1cbc257fda30cd58a175a32fc95a8ccd5cebdd3e7",
    "zh:91627f5af7e8315479a6c45cb1ae5db3c0a91a18018383cd409f3cfa04408aed",
    "zh:b5217a81cfc58334278831eacb2865bd8fc025b0cb1c576e9da9c4dc3a187ef5",
    "zh:c70afea4324518b099d23abc189dff22e6706ca0936de39eca01851e2550af7e",
    "zh:e62c212169ef9aad3b01f721db03b7e08d7d4acbbac67a713f06239a3a931834",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version     = "2.6.1"
  constraints = ">= 2.0.2"
  hashes = [
    "h1:xC0zk0wWTlWLwGLp9cBRJG8672Pp1PZQmHCA4FAJp4U=",
    "zh:081fbaf9441ebb278753dcf05f318fa7d445e9599a600d7c525e9a18b871d4c8",
    "zh:143bfbe871c628981d756ead47486e807fce876232d05607e0b8852ebee4eed8",
    "zh:34f413a644eb952e3f041d67ef19200f4c286d374eae87b60fafdd8bf6bb5654",
    "zh:370562be70233be730e1876d565710c3ef477e047f209cb3dff8a4a3217a6461",
    "zh:443021df6d56e59e4d8dda8e57b506affff32b8a22de09661d21b98bc781fefb",
    "zh:51a9501360b58adf9ee6e09fb81f555042ebc909ab36e06ccfc5e701e91f9923",
    "zh:7d41d48b8291b98e0a4b7a1f79a9d1fe140a2e0d8df422c5b48cbae4c3fa615a",
    "zh:881b3e44814d7d49a5820e2e4b13ee3d000b5baf7957df774a909f17472ece8a",
    "zh:b860ff68a944de63fbe0a624c41f2e373711a2da4298c0f0cb151e00fb32a6b3",
    "zh:c4ab48ea6e0f8d4a6db1abab1877addb2b21ecd126e505c74b8c85804bd92cbe",
    "zh:e96589575dfd31eab48fcc85466dd49895925473c60c802b346cdb4037953350",
  ]
}

provider "registry.terraform.io/hashicorp/tls" {
  version     = "3.1.0"
  constraints = "3.1.0"
  hashes = [
    "h1:fUJX8Zxx38e2kBln+zWr1Tl41X+OuiE++REjrEyiOM4=",
    "zh:3d46616b41fea215566f4a957b6d3a1aa43f1f75c26776d72a98bdba79439db6",
    "zh:623a203817a6dafa86f1b4141b645159e07ec418c82fe40acd4d2a27543cbaa2",
    "zh:668217e78b210a6572e7b0ecb4134a6781cc4d738f4f5d09eb756085b082592e",
    "zh:95354df03710691773c8f50a32e31fca25f124b7f3d6078265fdf3c4e1384dca",
    "zh:9f97ab190380430d57392303e3f36f4f7835c74ea83276baa98d6b9a997c3698",
    "zh:a16f0bab665f8d933e95ca055b9c8d5707f1a0dd8c8ecca6c13091f40dc1e99d",
    "zh:be274d5008c24dc0d6540c19e22dbb31ee6bfdd0b2cddd4d97f3cd8a8d657841",
    "zh:d5faa9dce0a5fc9d26b2463cea5be35f8586ab75030e7fa4d4920cd73ee26989",
    "zh:e9b672210b7fb410780e7b429975adcc76dd557738ecc7c890ea18942eb321a5",
    "zh:eb1f8368573d2370605d6dbf60f9aaa5b64e55741d96b5fb026dbfe91de67c0d",
    "zh:fc1e12b713837b85daf6c3bb703d7795eaf1c5177aebae1afcf811dd7009f4b0",
  ]
}

provider "registry.terraform.io/tyler-technologies/gitops" {
  version     = "0.0.2-rc"
  constraints = "0.0.2-rc"
  hashes = [
    "h1:l4nZMRNPODLiPvEtgMaI0NAVHH4dbhJSokJ6gWF/yhY=",
    "zh:164c4cb39f3c5503f83cc3819aab7b97ecad69eb3a5755c24129d92d0e2d8043",
    "zh:527cc37ec9c59b7247ea96f67f6e151b6cc51f769cac3abbf9afdbad00e993e7",
    "zh:591dd1a20e10afe7839c88478d335b7bad9cc6ebc2e3364ce3c0c0dac5681f6d",
    "zh:6be62a7a4c79b5f19928f3ef06f99e2c3a92477a90ab2237db1d0e996919a86e",
    "zh:9ef956a056c8b078b8edda85b20de83eee6758283f20c14db5c9240e75119fd8",
    "zh:a3ed339e6619ebcdb458ad6f56f24ae35112c691f884a56666103b04b57bb568",
    "zh:ccd827ab10cc0b4abc45629c95cbeb58a79cb797db96586f9ec5e53d244b4c8d",
    "zh:d0742ce505eb4e555d82336746cd110a2ee591eea0ea3ad1214ca1bc7a3b3cfb",
    "zh:d5394f672a0dcefcb21194d50f08eb25484aac5357c1f18d15819026ff50f898",
    "zh:ef208c3bfd50f9eaeff2ece02029ce824a12ac238f512b8160b3bd376da0a05e",
    "zh:ef5bcc5f4a9c59db9f3064a73564aebdb63846dcb0752e1edd899ddc1194798b",
  ]
}
